# APRS DTMF

module destiné à adapter un standard DTMF (touches clavier téléphone) à un système de report de données automatique (APRS).
Par © F4IEY Jules.

ATTENTION: <br>
Pour ceux qui souhaiteraient ajouter des modifications, **effectuez les dans une nouvelle branche** et faites une [merge request](https://gitlab.com/dkj3/aprs-dtmf/merge_requests) pour changer la version.
73 et bons projets.