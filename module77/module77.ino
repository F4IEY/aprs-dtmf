/*
 *  APRS DTMF par F4IEY Jules
 *  contains resources.h
 *  pin de sortie CW: 9
 *  pins DTMF: de 0 à 7 (bien lire la doc!)
 */
#include "resources.h"
#include <SoftwareSerial.h>
#include <TinyGPS.h>
unsigned const int rx = 10; //reception UART arduino
unsigned const int tx = 11; //transmission UART arduino
SoftwareSerial gpsWires(rx, tx);
TinyGPS gps; //on cree une instance gps
const int buttonPin = 8;
unsigned long DTpressedTime = 900; //valeur expérimentale à verifier
int bs = LOW;
unsigned long start = 0;
unsigned long depart = 0;
float lat, lon; 
String gpsDataFormat;
String DTid = (String)"1D" + (String)"3401"; //id "3401" à définir et remplacer

void setup() { //initialisation
  pinMode(buttonPin, INPUT);
  gpsWires.begin(9600); //connection UART avec le module
  if(gpsWires.available() && gps.encode(gpsWires.read())) gps.f_get_position(&lat, &lon); //encodage binaire et pointage sur les coordonnées
  //récupération des coordonnées GPS
  gpsDataFormat = gpsDTformat(lat, lon);
  ptt_init(); //on TX
  sendCW("CQCQ DE " + DTid + " = QTH QTH IS " + String(lat, 5) + "N, " + String(lon, 5) + "E = 73VA E E");
  delay(446); //easter egg
  sendDTMF("BCBC#" + DTid + "#" + gpsDataFormat + "#BCBC", DTpressedTime);
  ptt_release(); //on TX plus...
  delay(77); //another easter egg
  depart = millis(); //on lance le chrono!
}

void loop() {
  bs = digitalRead(buttonPin);
  //ne pas oublier de refresh les coordonnées GPS!
  if(gpsWires.available() && gps.encode(gpsWires.read())) gps.f_get_position(&lat, &lon);
  gpsDataFormat = gpsDTformat(lat, lon); //on convertit le GPS en DTMF
  start = millis();
  while(bs == HIGH){ //si le bouton est pressé pendant 3 secondes
    if((millis() - start) >= 2500) {
      delay(77);
      sendDTMF("BCBC#BAD505#BAD505#" + DTid + "#" + gpsDataFormat + "#BCBC", DTpressedTime); //on evoie un msg d'urgence après 2,5 secondes
      delay(77);
    }
  }
  if((millis() - depart) >= 600000) {
   delay(77); 
   sendDTMF("BCBC#" + DTid + "#" + gpsDataFormat + "#BCBC", DTpressedTime);
   delay(77);
   depart = millis(); //on reset le chrono 
  }
}
