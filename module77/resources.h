/*
	bibliothèque de fonctions de l'APRS DTMF par © F4IEY Jules
	Vous trouverez les prototypes de fonctions utilisées par le programme principal
*/

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

//PROTOTYPES:
/**
 *  Permet de sortir des commandes DTMF en utilisant les 8 premières pins de l'Arduino
 * @param la trame DTMF voulue de 0 à D pendant la duree souhaitee en ms
 * @see DTMFGen library
 */
void sendDTMF(String trame, unsigned long duree);

/**
 * Convertit les coordonnees GPS float en chaine de caracteres String adapte a l'utilisation du DTMF (les points remplaces par des * et les virgules par des #)
 * @param les coordonnes x et y
 * @return la chaine de caracteres avec les points remplaces par des * et un separateur # entre x et y
 */
String gpsDTformat(float y, float x);
/**
 * convertit les mots par minute en millisecondes
 */
unsigned long wpmToMillis(int wpm);
/**
 * contrôle PTT
 */
void ptt_release();
void ptt_init();
/**
 * CW
 */
void dot();
void dash();
/**
 * Envoie de la CW @param le texte a faire passer en majuscules
 */
void sendCW(String str);
